# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

default_app_config = 'treasury.apps.TreasuryConfig'
