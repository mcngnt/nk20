# Copyright (C) 2018-2024 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import lru_cache

from django import forms
from django.db import transaction
from django.db.models import Q

from .base import WEISurvey, WEISurveyInformation, WEISurveyAlgorithm, WEIBusInformation
from ...models import WEIMembership

WORDS = {
    "ambiance": ["Ambiance de bus :", {
        1: "Ambiance calme et posée",
        2: "Ambiance rigolage entre copaing",
        3: "Ambiance danse de camping autour d'une piscine inexistante",
        4: "Grosse soirée avec de la musique qui fait bouger",
        5: "On retourne le camping et le bus (dans le respect et le savoir vivre)"
    }],
    "musique": ["Musique :", {
        1: "Musique tranquille",
        2: "Musique commerciale",
        3: "Chansons paillardes",
        4: "Musique de Colonie de vacances",
        5: "Grosse techno"
    }],
    "boisson": ["Boissons :", {
        1: "Boisson soft",
        2: "Des cocktails de temps en temps",
        3: "Des coktails fancy de pétasse (parce que c'est les meilleurs)",
        4: "Bière !",
        5: "L'alcool c'est dans les céréales"
    }],
    "beauferie": ["Échelle de la beauferie :", {
        1: "Je suis toujours classe",
        2: "Je rote de temps en temps",
        3: "Claquette chaussette, c'est confortable",
        4: "L'aviron bayonnais est dans ma plyaylist",
        5: "Je suis champion⋅ne de concours de rots et d'éclatage de gobelet sur mon front"
    }],
    "sommeil": ["Échelle de ton sommeil pendant le WEI :", {
        1: "Dormir, c'est pour les faibles",
        2: "5h maximum",
        3: "10h",
        4: "15h",
        5: "Deux bonnes nuits de sommeil, c'est important pour être en forme pour les activités proposées par nos supers GC WEI"
    }],
    "vacances": ["Tes vacances de rêve :", {
        1: "Dans ma chambre",
        2: "Retourner chez popa et moman pour pouvoir enfin arrêter de manger des pasta box",
        3: "Être une grosse larve sous le soleil des troopiiiiiiiiques",
        4: "Faire un road trip camping sauvage, manger des racines et boire son pipi",
        5: "Le crime ne prend pas de vacances"
    }],
    "activite": ["T'as une heure de trou pendant ton WEI, que fais-tu ?", {
        1: "Je cherche des copaines pour faire un petit jeu de société",
        2: "Je cherche un moyen de me dépenser, n'importe quel ballon ferait l'affaire",
        3: "Je cherche un endroit où il y a de la musique pour bouger sur le dancefloor",
        4: "Petit apéro, petite pétanque avec les collègues autour d'un bon pastaga",
        5: "Je cherche une connerie à faire (mais pas trop méchante, pour ne pas embêter mes GC WEI préférés)"
    }],
    "hygiene": ["Échelle de ton hygiène :", {
        1: "La douche, c'eest tous les jours",
        2: "La règle des 2 jours, c'est un droit et un devoir",
        3: "Je ne me lave qu'après le sport",
        4: "« Ne vous inquiétez pas, je pue pas »",
        5: "Y a que les sales qui se lavent"
    }],
    "animal": ["Tu décrirais ton animal totem plutôt comme :", {
        1: "Un dragon qui raserait des villes entières d'un seul souffle",
        2: "Une mouette qui pique des frites aux dunkerquois",
        3: "Un poulpe tout meunion",
        4: "Un pitbull qui au fond cache un petit cœur en sucre",
        5: "Un canard en plastique au bord d'une baignoire qui n'a pas servi depuis 10 ans"
    }],
    "fensfoire": ["Quel est ton rapport à la F[ENS]foire ?", {
        1: "Je réveille les autres à 6h avec mon instrument",
        2: "Je la suis partout",
        3: "J'aime bien l'écouter de temps en temps",
        4: "Je mets des bouchons d'oreilles pour ne pas l'entendre",
        5: "La quoi ?"
    }],
    "kokarde": ["Qu'est-ce que le mot Kokarde t'évoque ?", {
        1: "Vraiment pas mon truc les soirées…",
        2: "Bof, je viens pour manger et je repars aussitôt",
        3: "Je kiffe, good vibes",
        4: "Perso, je ne m'arrêterai pas de danser sur la piste !",
        5: "J'resterai jusqu'à 3h ou rien"
    }],
    "copain": ["Qu'est-ce que tu fais avec un⋅e «copain⋅ine» ?", {
        1: "Je l'insulte de sale merde",
        2: "J'lui fais faire des trucs cons et je l'affiche !",
        3: "On parlerait ensemble et on se marrerait",
        4: "On aurait des vrais gros délires",
        5: "Je meurs pour lui/elle"
    }],
    "vie": ["Selon toi, qu'est-ce que la vie ?", {
        1: "La vie, cette sale race !",
        2: "Un moment paisible avant la mort",
        3: "C'est difficile à définir...",
        4: "En vrai, c'est cool !",
        5: "Une gigantestque tranche de kiff ! Et tous les jours, j'en mange un morceau"
    }],
    "jeux": ["Quel est ton rapport avec les jeux de société ?", {
        1: "éloigné",
        2: "nonchalant",
        3: "timide",
        4: "assumé",
        5: "sexuel"
    }],
    "calin": ["Qu'est-ce que tu penses des câlins ?", {
        1: "Jamais je n'en fais et jamais je n'en ferai !",
        2: "J'en fais mais ça ne me plaît pas",
        3: "J'en fais rarement mais c'est toujours cool",
        4: "J'en fais tous les jours avec mes ami⋅es !",
        5: "Je pourrais en faire à n'importe qui. Pourquoi ne pas créer le club Câl[ENS] ?"
    }],
    "vomi": ["Quel est ton rapport au vomi ?", {
        1: "C'est compliqué…",
        2: "Jamais je ne vomis mais je nettoie quand mes potes vomissent",
        3: "Jamais je ne vomis et jamais je ne nettoie celui de quelqu'un d'autre",
        4: "Je vomis quelquefois, ça arrive, faites pas cette tête, mais je fins toujours par nettoyer !",
        5: "Je vomis à chaque soirée et ce n'est jamais moi qui nettoie"
    }],
    "kfet": ["Qu'est ce que la Kfet t'évoque ?", {
        1: "La Kfet, quel lieu de dépravé⋅es sérieux…",
        2: "C'est un endroit à l'hygiène plus que douteuse…",
        3: "Téma les prix des boissons et des snacks, c'est aberrant !",
        4: "En vrai, c'est cool, petit billard, petit canapé, chill !",
        5: "Banger, j'y reste jusqu'à la fin de mes jours"
    }],
    "fatigue": ["Comment combattre la fatigue lors de ton WEI ?", {
        1: "Le sport en journée, ça réveille",
        2: "Le sucre du coca, ça réveille",
        3: "La taurine du Red Bull, ça réveille",
        4: "L'alcool dans le sang, ça réveille",
        5: "L'écocup sur le front, ça réveille"
    }],
    "duree trajet": ["Quelle serait ta durée de trajet préférée ?", {
        1: "Trajet instantané, pas le temps de niaiser",
        2: "1h, histoire de faire connaissance avec quelques personnes avant de se jeter sur les boissons",
        3: "3h, on peut vraiment parler et apprendre à connaître nos voisin⋅es",
        4: "6h, histoire d'avoir le temps de faire des conneries dans le bus pour bien se marrer !",
        5: "12h, il faut bien trouver un moment pour dormir, ce seront deux gros dodos dans un bus"
    }],
    "scolarite": ["Comment tu vois ton cursus à l'ENS ?", {
        1: "La tranquillité et le travail",
        2: "On va s'amuser tout en bossant",
        3: "Ça va profiter et réviser au dernier moment pour les exams…",
        4: "Nous festoierons sans songer aux conséquences",
        5: "Je ne vois qu'une seule issue : la débauche"
    }]
}


class WEISurveyForm2023(forms.Form):
    """
    Survey form for the year 2023.
    Members answer 20 questions, from which we calculate the best associated bus.
    """
    def set_registration(self, registration):
        """
        Filter the bus selector with the buses of the current WEI.
        """
        information = WEISurveyInformation2023(registration)

        question = information.questions[information.step]
        self.fields[question] = forms.ChoiceField(
            label=WORDS[question][0],
            widget=forms.RadioSelect(),
        )
        answers = [(answer, WORDS[question][1][answer]) for answer in WORDS[question][1]]
        self.fields[question].choices = answers


class WEIBusInformation2023(WEIBusInformation):
    """
    For each question, the bus has ordered answers
    """
    scores: dict

    def __init__(self, bus):
        self.scores = {}
        for question in WORDS:
            self.scores[question] = []
        super().__init__(bus)


class WEISurveyInformation2023(WEISurveyInformation):
    """
    We store the id of the selected bus. We store only the name, but is not used in the selection:
    that's only for humans that try to read data.
    """

    step = 0
    questions = list(WORDS.keys())

    def __init__(self, registration):
        for question in WORDS:
            setattr(self, str(question), None)
        super().__init__(registration)


class WEISurvey2023(WEISurvey):
    """
    Survey for the year 2023.
    """

    @classmethod
    def get_year(cls):
        return 2023

    @classmethod
    def get_survey_information_class(cls):
        return WEISurveyInformation2023

    def get_form_class(self):
        return WEISurveyForm2023

    def update_form(self, form):
        """
        Filter the bus selector with the buses of the WEI.
        """
        form.set_registration(self.registration)

    @transaction.atomic
    def form_valid(self, form):
        self.information.step += 1
        for question in WORDS:
            if question in form.cleaned_data:
                answer = form.cleaned_data[question]
                setattr(self.information, question, answer)
        self.save()

    @classmethod
    def get_algorithm_class(cls):
        return WEISurveyAlgorithm2023

    def is_complete(self) -> bool:
        """
        The survey is complete once the bus is chosen.
        """
        for question in WORDS:
            if not getattr(self.information, question):
                return False
        return True

    @lru_cache()
    def score(self, bus):
        if not self.is_complete():
            raise ValueError("Survey is not ended, can't calculate score")

        bus_info = self.get_algorithm_class().get_bus_information(bus)
        # Score is the given score by the bus subtracted to the mid-score of the buses.
        s = 0
        for question in WORDS:
            s += bus_info.scores[question][str(getattr(self.information, question))]
        return s

    @lru_cache()
    def scores_per_bus(self):
        return {bus: self.score(bus) for bus in self.get_algorithm_class().get_buses()}

    @lru_cache()
    def ordered_buses(self):
        values = list(self.scores_per_bus().items())
        values.sort(key=lambda item: -item[1])
        return values

    @classmethod
    def clear_cache(cls):
        return super().clear_cache()


class WEISurveyAlgorithm2023(WEISurveyAlgorithm):
    """
    The algorithm class for the year 2023.
    We use Gale-Shapley algorithm to attribute 1y students into buses.
    """

    @classmethod
    def get_survey_class(cls):
        return WEISurvey2023

    @classmethod
    def get_bus_information_class(cls):
        return WEIBusInformation2023

    def run_algorithm(self, display_tqdm=False):
        """
        Gale-Shapley algorithm implementation.
        We modify it to allow buses to have multiple "weddings".
        """
        surveys = list(self.get_survey_class()(r) for r in self.get_registrations())  # All surveys
        surveys = [s for s in surveys if s.is_complete()]  # Don't consider invalid surveys
        # Don't manage hardcoded people
        surveys = [s for s in surveys if not hasattr(s.information, 'hardcoded') or not s.information.hardcoded]

        # Reset previous algorithm run
        for survey in surveys:
            survey.free()
            survey.save()

        non_men = [s for s in surveys if s.registration.gender != 'male']
        men = [s for s in surveys if s.registration.gender == 'male']

        quotas = {}
        registrations = self.get_registrations()
        non_men_total = registrations.filter(~Q(gender='male')).count()
        for bus in self.get_buses():
            free_seats = bus.size - WEIMembership.objects.filter(bus=bus, registration__first_year=False).count()
            # Remove hardcoded people
            free_seats -= WEIMembership.objects.filter(bus=bus, registration__first_year=True,
                                                       registration__information_json__icontains="hardcoded").count()
            quotas[bus] = 4 + int(non_men_total / registrations.count() * free_seats)

        tqdm_obj = None
        if display_tqdm:
            from tqdm import tqdm
            tqdm_obj = tqdm(total=len(non_men), desc="Non-hommes")

        # Repartition for non men people first
        self.make_repartition(non_men, quotas, tqdm_obj=tqdm_obj)

        quotas = {}
        for bus in self.get_buses():
            free_seats = bus.size - WEIMembership.objects.filter(bus=bus, registration__first_year=False).count()
            free_seats -= sum(1 for s in non_men if s.information.selected_bus_pk == bus.pk)
            # Remove hardcoded people
            free_seats -= WEIMembership.objects.filter(bus=bus, registration__first_year=True,
                                                       registration__information_json__icontains="hardcoded").count()
            quotas[bus] = free_seats

        if display_tqdm:
            tqdm_obj.close()

            from tqdm import tqdm
            tqdm_obj = tqdm(total=len(men), desc="Hommes")

        self.make_repartition(men, quotas, tqdm_obj=tqdm_obj)

        if display_tqdm:
            tqdm_obj.close()

        # Clear cache information after running algorithm
        WEISurvey2023.clear_cache()

    def make_repartition(self, surveys, quotas=None, tqdm_obj=None):
        free_surveys = surveys.copy()  # Remaining surveys
        while free_surveys:  # Some students are not affected
            survey = free_surveys[0]
            buses = survey.ordered_buses()  # Preferences of the student
            for bus, current_score in buses:
                if self.get_bus_information(bus).has_free_seats(surveys, quotas):
                    # Selected bus has free places. Put student in the bus
                    survey.select_bus(bus)
                    survey.save()
                    free_surveys.remove(survey)
                    break
                else:
                    # Current bus has not enough places. Remove the least preferred student from the bus if existing
                    least_preferred_survey = None
                    least_score = -1
                    # Find the least student in the bus that has a lower score than the current student
                    for survey2 in surveys:
                        if not survey2.information.valid or survey2.information.get_selected_bus() != bus:
                            continue
                        score2 = survey2.score(bus)
                        if current_score <= score2:  # Ignore better students
                            continue
                        if least_preferred_survey is None or score2 < least_score:
                            least_preferred_survey = survey2
                            least_score = score2

                    if least_preferred_survey is not None:
                        # Remove the least student from the bus and put the current student in.
                        # If it does not exist, choose the next bus.
                        least_preferred_survey.free()
                        least_preferred_survey.save()
                        free_surveys.append(least_preferred_survey)
                        survey.select_bus(bus)
                        survey.save()
                        free_surveys.remove(survey)
                        break
            else:
                raise ValueError(f"User {survey.registration.user} has no free seat")

            if tqdm_obj is not None:
                tqdm_obj.n = len(surveys) - len(free_surveys)
                tqdm_obj.refresh()
